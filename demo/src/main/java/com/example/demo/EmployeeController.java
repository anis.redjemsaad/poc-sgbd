package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();

    }

    @GetMapping("/Add")
    public String createEmployee() {

        try{
            employeeRepository.save(new Employee("toto","tata"));
            employeeRepository.flush();
            return "ok";
        }catch(Exception e){
            return "oops";
        }
    }

}